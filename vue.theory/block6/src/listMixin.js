export default {
	data () {
		return {
			searchName: '',
			names: ['Vanya', 'Olezhka', 'Denchyk'],
		}
	},
	computed: {
		filteredNames() {
			return this.names.filter(name => {
				return name.toLowerCase().indexOf(this.searchName.toLowerCase()) !== -1
			})
		}
	}
}
