import VueRouter from 'vue-router'
import Home from './pages/Home'
import CarFull from './pages/CarFull'
import ErrorComponent from './pages/Error'

const Cars = resolve => {
	require.ensure(['./pages/Cars.vue'], () => {
		resolve(
			require('./pages/Cars.vue')
		);
	});
};

const Car = resolve => {
	require.ensure(['./pages/Car.vue'], () => {
		resolve(
			require('./pages/Car.vue')
		);
	});
};

export default new VueRouter({
	routes: [
		{
			path: '',
			component: Home,
		},
		{
			path: '/cars',
			component: Cars,
			name: 'Cars',
		},
		{
			path: '/car/:id',
			component: Car,
			children: [
				{
					path: 'full',
					component: CarFull,
					name: 'CarFull',
					beforeEnter(to, from, next) {
						/*
						if(true) {
							next(true);
						} else {
							next(false);
						}
						*/
						next();
					}
				}
			],
		},
		{
			path: '/none',
			// redirect: '/cars',
			redirect: {
				name: 'Cars',
			},
		},
		{
			path: '*',
			component: ErrorComponent,
		},
	],
	mode: 'history',
	scrollBehavior(to, from, savedPosition) {

		if(savedPosition) {
			return savedPosition;
		}

		if(to.hash) {
			return {
				selector: to.hash,
			}
		}

		return {
			x: 0,
			y: 300,
		}
	},
})
