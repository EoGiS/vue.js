export default {
	bind(element, bindings, virtualNode) {
		const fontModifier = bindings.modifiers['font'];
		if(fontModifier) {
			element.style.fontSize = '30px';
		}

		const delayModifier = bindings.modifiers['delay'];
		let delay = 0;
		if(delayModifier) {
			delay = 2000;
		}

		setTimeout(() => {
			const arg = bindings.arg;
			element.style[arg] = bindings.value;
		}, delay);
	},
}
