function Car(carName) {
    this.carName = carName || "Невідоме ім'я";
}

Car.prototype.logName = function () {
    console.log('Car name is: ', this.carName);
};

/*
module.exports = {
    Car: Car
};
*/

/*
function callConsole() {
    console.log(arguments)
}
*/

// module.exports.CarClass = Car;
global.CarClass = Car;