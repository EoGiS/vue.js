//////////////// lesson 1 ////////////////

/*
text = "Hello world";

console.log(text);
*/

//////////////// lesson 1 ////////////////


//////////////// lesson 2 ////////////////

// Car = require('./car').CarClass;

// bmw = new Car('BMW');
// bmw.logName();

// require('./car');

// bmw = new CarClass('BMW');
// bmw.logName();

//////////////// lesson 2 ////////////////


//////////////// lesson 3 ////////////////

// lodash = require('./node_modules/lodash/lodash');
// lodash = require('lodash');
// console.log(lodash.sum([4, 6, 7]));

//////////////// lesson 3 ////////////////


//////////////// lesson 4 ////////////////

// lodash = require('lodash');
// console.log(lodash.sum([4, 6]));

//////////////// lesson 4 ////////////////


//////////////// lesson 6 ////////////////

/*
util = require('util');

function Car() {

}

Car.prototype.logName = function () {
    console.log("This name is: " + this.name);
};

function BMW(name) {
    this.name = name || "Unknown BMW model";
}

BMW.prototype.drive = function () {
    console.log("I'm driving");
};

util.inherits(BMW, Car);

bmw = new BMW('x6');
bmw.logName();
*/

/*
EventEmitter = require('events').EventEmitter;
dispatcher = new EventEmitter();

dispatcher.on('connect', function (data) {
    console.log('connect 1 ', data);
});

dispatcher.on('connect', function (data) {
    console.log('connect 2 ', data);
});

dispatcher.on('error', function (err) {
   console.log('Error');
});

dispatcher.emit('connect', {foo : 1});
dispatcher.emit('error', new Error('something went wrong'));
*/

//////////////// lesson 6 ////////////////


//////////////// lesson 7 ////////////////

fs = require('fs');

// fs.writeFileSync('test.txt', 'Hello World');
/*
data = fs.readFileSync('test.txt');
console.log(data.toString());
*/
/*
data = fs.readFileSync('test.txt', {encoding : 'UTF-8'});
console.log(data);
*/

/*
fs.writeFile('test.txt', 'test', function (error) {
    if(error) throw new Error(error);

    fs.readFile('test.txt', { encoding: 'UTF-8'}, function(error, data){
        if(error) throw new Error(error);

        console.log(data);
    })
});
console.log('12');
*/

/*
fs.writeFile('test.txt', 'test', function (error) {
    if(error) throw new Error(error);

    fs.rename('test.txt', 'test2.txt', function (error) {
        if(error) throw new Error(error);

        fs.readFile('test2.txt', { encoding: 'UTF-8'}, function(error, data){
            if(error) throw new Error(error);

            console.log(data);
        })
    });
});
*/

//////////////// lesson 7 ////////////////


//////////////// lesson 8 ////////////////

/*
http = require('http');
server = new http.Server;

server.listen(80, '127.0.0.1');

server.on('request', function (request, response) {
    // console.log(request, response);
    response.end("Hello, I'm server");
});
*/

//////////////// lesson 8 ////////////////


//////////////// lesson 9 ////////////////

/*
http = require('http');
url = require('url');
fs = require('fs');

server = new http.Server;

server.listen(80, '127.0.0.1');

server.on('request', function (request, response) {
    parsed_url = url.parse(request.url, true);

    // response.end(parsed_url.query.q);

    fs.readFile(getPageNameByPath(parsed_url.pathname) + '.html', function (error, data) {
        if (error) throw new Error(error);

        response.end(data);
    })
});

function getPageNameByPath(path) {
    switch (path) {
        case '/':
        case '/home':
            return 'index';
        case '/about':
            return 'about';
        default:
            return 'error';
    }
}
*/

//////////////// lesson 9 ////////////////


//////////////// lesson 10 ////////////////

/*
for (i = 2; i < process.argv.length; i++) {
    console.log(process.argv[i]);
}
*/

/*
optimist = require('optimist');

// console.log(optimist.argv);

message = optimist.argv.message;

console.log("Hello, " + message);       //      node index --message=Vanya
*/

//////////////// lesson 10 ////////////////