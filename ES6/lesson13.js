/*
export let name = `Vanya`;
export let age = 20;
*/

/*
let name = 'Vanya';
let age = 20;

export {name, age};
*/

/*
export function func() {
    console.log('function');
}
*/

/*
export default function func() {
    console.log('function');
}
*/

/*
export default class Car {
    constructor() {
        console.log('Car constructor');
    }
}
*/

// import export