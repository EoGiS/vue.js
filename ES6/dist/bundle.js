/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
///////////////// lesson 2 ////////////////

/*
const MY_CONST = [1, 2, 3];
MY_CONST[2] = 4;

const MY_OBJ = {
    name: 'Vanya',
};
MY_OBJ.name = 'Other name';

let a = 1;
a = 'sad';

for(let i = 0; i < 5; i++) {
    setTimeout(function () {
        console.log(i);
    }, 1000);
}

const PI = 3.14;
let b = 1;
*/

///////////////// lesson 2 ////////////////


///////////////// lesson 3 ////////////////

/*
const original = function () {
    return 100;
};
*/

// console.log(original());

/*
const arrow = (num) => {
    return 150 + num;
};
*/

// const sum = (a, b) => a + b;
// const test = a => a * 5;

/*
const obj = {
    name: 'WFM',
    logName: function() {
        setTimeout(() => {
            console.log(this.name);
        }, 1000);
    }
};

obj.logName();
*/

// console.log(test(21));

///////////////// lesson 3 ////////////////


///////////////// lesson 4 ////////////////

/*
const func = (a = 5, b = 16) => {
    return a + b;
};

console.log(func(undefined, 13));
*/

///////////////// lesson 4 ////////////////


///////////////// lesson 5 ////////////////

/*
const name = 'WFM';
const age = '35';

const obj = {name, age};

console.log(obj);
*/

/*
const create_person = (name, surname, field_name, fieldPostfix) => {
    const full_name = name + ' ' + surname;
    return {
        full_name,
        name,
        surname,
        getJob() {
            return 'front end';
        },
        [field_name + fieldPostfix] : 'test value',
    };
};

const person = create_person('test', 'test2', 'car', '-name');
console.log(person);
*/

///////////////// lesson 5 ////////////////


///////////////// lesson 6 ////////////////

/*
let obj = {
    name: 'WFM',
    age: 20,
};

// let {name, age} = obj;
let {name:n, age:a} = obj;

console.log(n, a);
*/

/*
let array = ['WFM', 30, 'red'];
let [name, age, color='yellow'] = array;
console.log(name, age, color);
*/

///////////////// lesson 6 ////////////////


///////////////// lesson 7 ////////////////

/*
function logString(num, ...args) {
    console.log(num, args);
}

logString(20, 'wfm', 'str2');
*/

/*
function logString(num, ...args) {
    console.log(num, args);
}

let spreadArray = ['wfm', 'str2', 'sadfsd'];

logString(20, ...spreadArray);
*/

///////////////// lesson 7 ////////////////


///////////////// lesson 8 ////////////////

// let name = "Test";
// let str = `Hello ${name}, glad "to" 'see' you ${10 + 15}`;

/*
let html = `
    <div>
        <h1>${name}</h1>
        <span>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, fugiat.</p>
        </span>
    </div>
`;
console.log(html);
*/

///////////////// lesson 8 ////////////////


///////////////// lesson 9 ////////////////

/*
const array = [1, 2, 3, 4, 5];

for (let item of array) {
    console.log(item);
}
*/

///////////////// lesson 9 ////////////////


///////////////// lesson 10 ////////////////

/*
class Car {
    constructor(name) {
        console.log('Car constructor');

        this.name = name;
    }

    logName() {
        console.log(this.name);
    }

    static staticFunc(){
        console.log('static');
    }
}

class BMW extends Car {
    constructor(name) {
        super(name);

        console.log('BMW constructor');
    }
}

let bmw = new BMW('X6');
bmw.logName();
BMW.staticFunc();
*/

///////////////// lesson 10 ////////////////


///////////////// lesson 11 ////////////////

/*
let set = new Set();
set.add(10);
set.add('Hello');
set.add({});
set.add(10);
// console.log(set.size);
*/

/*
let set = new Set([1, 2, 3, 4, 3, 2]);
console.log(set);
*/

// let set = new Set().add(2).add('234').add(3);
// console.log(set.has(3));
// set.clear();
// console.log(set.delete('234'));

/*
let set = new WeakSet();
let key = {};

set.add(key);
console.log(set.size);
key = null;
console.log(set.size);
*/

///////////////// lesson 11 ////////////////


///////////////// lesson 12 ////////////////

/*
let map = new Map();
map.set('name', 'test name');
map.set('age', '20');
// console.log(map);
// console.log(map.get('age'));

let obj1 = {};
let obj2 = {};

map.set(obj1, 13);
map.set(obj2, 23);

console.log(map.size);
console.log(map.has(obj2));
console.log(map.delete(obj2));
*/

/*
let map = new Map([
   ['name', 'Vanya'],
   ['age', 20],
]);

console.log(map);

for (let item of map.values()) {
    console.log(item);
}

for (let key of map.keys()) {
    console.log(key);
}

for (let arr of map.entries()) {
    console.log(`${arr[0]} - ${arr[1]}`);
}
*/

///////////////// lesson 12 ////////////////


///////////////// lesson 13 ////////////////

// import * as array from './lesson13';
// import {name as n, age as a, func} from './lesson13';

// console.log(n, a);
// func();

// import func from './lesson13';
// func();

// import Car from './lesson13';
// let car = new Car();

///////////////// lesson 13 ////////////////


///////////////// lesson 14 ////////////////

// let sym = Symbol('name');
// let sym2 = Symbol('test');
// let sym3 = Symbol('test');

// console.log(sym2 === sym3);  false

/*
let sym = Symbol('field');
let obj = {
    age: 20,
    [sym]: 'test',
};
*/

// console.log(obj[sym]);
// console.log(Object.getOwnPropertyNames(obj));
// console.log(Object.getOwnPropertySymbols(obj));

/*
let num = 1;
let str = 'test';
let arr = [1, 2, 3];
let obj = {name: 'WFM'};

console.log(typeof num[Symbol.iterator]);
console.log(typeof str[Symbol.iterator]);
console.log(typeof arr[Symbol.iterator]);
console.log(typeof obj[Symbol.iterator]);
*/

/*
function createIterator(arr) {
    let count = 0;
    return {
        next() {
      p      return count < arr.length
                ? { value: arr[count++], done: false }
                : { value: undefined, done: true };
        }
    };
}
let iter = createIterator([1, 2, 3, 4]);
console.log(iter.next());
console.log(iter.next());
console.log(iter.next());
console.log(iter.next());
console.log(iter.next());
console.log(iter.next());
*/

/*
let fib = {
    [Symbol.iterator]() {
        let pre = 0, cur = 1;
        return {
            next() {
                [pre, cur] = [cur, pre + cur];
                return { value: cur, done: false };
            }
        }
    }
};

for (let n of fib) {
    if (n > 1500) break;
    console.log(n);
}
*/

///////////////// lesson 14 ////////////////


///////////////// lesson 15 ////////////////

/*
function* gen() {
    yield 11;
    yield 22;
    yield 33;
}

let generator = gen();
*/

/*
function* g1() {
    yield 1;
    yield* g2();
    yield 4;
}

function* g2() {
    yield 2;
    yield 3;
}

let generator = g1();
*/

/*
function* g() {
    yield* [1, 2, 3];
}

let generator = g();
console.log(generator.next());
console.log(generator.next());
console.log(generator.next());
console.log(generator.next());
console.log(generator.next());
*/

/*
function* getRange(start = 0, end = 100, step = 10) {
    while (start < end) {
        yield start;
        start += step;
    }
}

for (let n of getRange(10, 50)) {
    console.log(n);
}
*/

/*
function* fib(pre = 0, cur = 1) {
    while (cur < 10000) {
        [cur, pre] = [cur + pre, cur];
        yield cur;
    }
}
for (let n of fib()) {
    console.log(n);
}
*/

///////////////// lesson 15 ////////////////


///////////////// lesson 16 ////////////////

/*
let obj1 = { a: 1 };
let obj2 = { b: 2, c: 3 };

// Object.assign(obj1, obj2);
// console.log(obj1);
// console.log(obj2);

let obj3 = Object.assign({}, obj1, obj2);
console.log(obj3);
*/

/*
let arr = [1, 2, 3, 4, 5, 6];
console.log(arr.find(x => x > 3));
*/

// let str = "Hello";
// console.log(str.repeat(3));
// console.log(str.startsWith('Hel'));
// console.log(str.startsWith('ll', 3));
// console.log(str.includes('ello'));

///////////////// lesson 16 ////////////////


///////////////// lesson 17 ////////////////

/*
function oldDelay(ms, func) {
    setTimeout(function () {
        func();
    }, ms);
}

oldDelay(3000, function () {
    console.log('Old delay test');
});

console.log('test');
*/

/*
function delay(ms = 1000) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject();
        }, ms)
    });
}

delay(4000)
    .then(() => {
        console.log('new delay test');
    })
    .catch(() => {
        console.warn('error');
    });
*/

/*
import $ from 'jquery';

let promise = new Promise((resolve, reject) => {
    $.ajax({
        url: 'http://date.jsontest.com/',
        dataType: 'json',
        success: function (response) {
            resolve(response);
        },
        error: function (error) {
            reject(error);
        }
    });
});

promise
    .then((data) => {
        return data.date;
    })
    .then((date) => {
        console.log(date);
    })
    .catch((error) => {
        console.info(error);
    });
*/

///////////////// lesson 17 ////////////////


/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map?654a6f74deef3c0140fe