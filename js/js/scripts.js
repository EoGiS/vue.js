// ---------- lesson 1 ----------

// alert('Test');

// var i = 10;

// Alert('test'); - error

// var a;
// a=3;

// a = function () {
  // code
// };

// alert(a);

/*
10, 20.32
'hello', "hello"
true false
null
undefined
*/

// ---------- lesson 1 ----------

// ---------- lesson 2 ----------

/*
numbers
20
40.17
0xAA - 16-ова система числення
10.5e-3     10.5 * 10^-3
*/

/*
mathematics operators
+
-
/
*
% - залишок від ділення
*/

/*
strings
'hello'
"hel 'hello' lo"
*/

// alert(10.5e-3);


// ---------- lesson 2 ----------


// ---------- lesson 3 ----------

// a = 10;

// 1, 2, 3, Infinity, -Infinity, 'string' => TRUE
// 0 -0 null NaN Undefined => FALSE

/*
if(10){
    var b = 12;
    var c = a + b;
    alert(c);
    // alert('hello');
}
*/

/*
if (a > 10) {
    console.log(a);
} else if (a) {
    console.log('else-if');
}
*/

// a = 9;

/*
switch (a) {
    case 10:
        alert('a = 10');
        break;

    case 5:
        alert('a = 5');
        break;

    default:
        document.write('default');
        break;
}
*/

// a=5;

/*
while (a < 10){
    console.log(a);
    a++;
}
*/

/*
var c=2;
i=0;

do {
    c = c*2;
    i++;
    console.log(c);
}
while (i > 9);
*/

/*
for (j = 0; j < 10; j++) {
    console.log(j);
}
*/

// ---------- lesson 3 ----------


// ---------- lesson 4 ----------

/*
function sum(a, b) {

    function multiplication(a, b) {

        return a*b;
    }

    return a + multiplication(a,b);
}
*/

// console.log(sum(11,19));

/*
var func = function (a, b) {
    console.log(a + b);
};
*/

/*
(function (a, b) {
    console.log(a + b);
} (10,30));
*/

/*
var f = function fact(a = 5) {
    if(a <= 1)
        return 1;

    return a * fact((a - 1));
};

console.log(f(6));
*/

// ---------- lesson 4 ----------


// ---------- lesson 5 ----------

// var arr = [1, 2];
// var arr2 = [1, 2, 3, 4, arr];

// var arr = new Array(1, 2, 3);
// var arr = new Array(5);

// var arr = [1, 2, 3, 4, 5];

/*
arr[2] = 'hello';
arr[arr[2]] = 'word';
arr[2.43] = 'hello';
arr[-2.43] = 'hello';
*/

// arr[38] = 38;
// arr[100] = 100;

/*
for (i=0; i < arr.length; i++)
    console.log(arr[i]);
    */

// console.log(Object.keys(arr));
// keys = Object.keys(arr);

// console.log(arr);

// ---------- lesson 5 ----------


// ---------- lesson 6 ----------

// arr = [9, 2, 3, 4, 8, 'test', 'a_test'];

// 5 in arr             чи є в масиві arr індекс 5
// Array.isArray(arr)
// arr.join()           масив в рядок через ","
// arr.reverse()        масив в зворотньому порядку
// arr.sort()           сортування масиву (в алфавітному порядку)

/*                      сортування за зростанням
arr.sort(function(a, b) {
   return a-b;
})
*/

/*                      сортування за спаданням
arr.sort(function(a, b) {
   return b-a;
})
*/

// arr.concat('sad', 5, '324')  додавання до елементів масиву
// arr.slice(1, 4)              отримуємо масив з елментів 1-3 (не включаємо 4)
// arr.splice(1, 2)             видалення двох елементів починаючи з 1
// arr.splice(1, 2, '231')      видалення двох елементів починаючи з 1 і додавання нових
// arr.push(1, 214, '231')      додає елементи в кінець масиву і повертає нову довжину
// arr.pop()                    повертає і видаляє останній елемент
// arr.unshift('21', 'tdr')     додає елементи на початок масиву
// arr.shift()                  повертає і видаляє перший елемент
// delete(arr[2]);              видалення елементу без зміщення
/*
arr.forEach(function (value, index, array) {
    array[index] = value + 5 + 'hello';
});
*/
/*
new_arr = arr.map(function (value, index, array) {
    return value*value;
});
*/
/*
new_arr = arr.filter(function (value) {
   return value > 5;
});
*/
/*                              перевіряє чи всі елементи задовольняють елементи
arr2 = arr.every(function (value) {
    return value < 5;
});
*/
/*                              перевіряє чи хоча б один елемент задовольняє умову
arr2 = arr.some(function (value) {
    return value < 5;
});
*/
/*                              сума всіх елементів масиву
arr2 = arr.reduce(function (previousValue, currentValue) {
    return previousValue + currentValue;
}, 0);
*/
/*                              сума в зворотньому порядку
arr2 = arr.reduceRight(function (previousValue, currentValue) {
    return previousValue + currentValue;
}, 0);
*/
// arr.indexOf('test')          повертає індекс елементу

// console.log(arr.indexOf('test'));
// console.log(arr2);

// ---------- lesson 6 ----------


// ---------- lesson 7 ----------

// date = new Date();
// date = Date();

// date = new Date(32994239942342);
// date = new Date('04.09.2018');
// date = new Date(1998, 3, 9, 14, 55, 30);    3 - квітень

date = new Date();

days = ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота'];
monthes = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'];

// date.getDate();                  число
// date.getDay();                   день тижня: неділя - 0, понеділок - 1
// date.getFullYear();              рік
// date.getHours();                 години 0-23
// date.getMilliseconds();          мілісекунди
// date.getMinutes();               хвилини 0-59
// date.getMonth();                 місяць 0-11
// date.getSeconds();               секунди 0-59
// date.getTime();                  кількість мілісекунд від 1970
// date.getTimezoneOffset();        кількість хвилин різниці з Грінвичем

// date.setDate(30);                число
// date.setFullYear(2014);          рік
// date.setFullYear(2014, 7, 21);   рік, місяць, число
// date.setHours(22);               години
// date.setHours(22, 14, 45, 64);   години, хвилини, секунди, мілісекунди
// date.setMilliseconds(245);       мілісекунди
// date.setMinutes(41);             хвилини
// date.setMinutes(41, 14, 453);    хвилини, секунди, мілісекунди
// date.setMonth(11, 28);           місяць, число
// date.setTime(991646494847);      кількість мілісекунд від 1970

// date.toDateString()              Sun Jun 10 2018
// date.toUTCString()               Sun, 10 Jun 2018 07:42:59 GMT   (-3)
// date.toJSON()                    2018-06-10T07:43:34.383Z
// date.toLocaleDateString()        10.06.2018
// date.toLocaleString()            10.06.2018, 10:45:29
// date.toLocaleTimeString()        10:45:51
// date.toString()                  Sun Jun 10 2018 10:46:10 GMT+0300 (Фінляндія (літо))
// date.toTimeString()              10:46:33 GMT+0300 (Фінляндія (літо))

// Date.now()                       поточна мітка UNIX
// Date.parse(date.toString())      мітка UNIX для дати date

function display_time() {
    now = new Date();
    div = document.getElementById('clock');

    div.innerHTML = now.toLocaleTimeString();

    setTimeout(display_time, 1000);     // запуск фукнції display_time щосекунди
}

function countdown() {
    now = new Date();
    div = document.getElementById('countdown');
    date = new Date(2018, 11, 31, 23, 59, 59);

    timer = date.getTime() - now.getTime();

    days = parseInt(timer / (24 * 60 * 60 * 1000));
    hours = parseInt(timer / (60 * 60 * 1000)) % 24;
    minutes = parseInt(timer / (60 * 1000)) % 60;
    seconds = parseInt(timer / (1000)) % 60;

    div.innerHTML = days + ":" + hours + ":" + minutes + ":" + seconds;

    setTimeout(countdown, 1000);     // запуск фукнції display_time щосекунди
}

// window.onload = display_time;            // коли сторінка повністю завантажена
// window.onload = countdown;              // коли сторінка повністю завантажена

// ---------- lesson 7 ----------


// ---------- lesson 8 ----------

function string_work() {
    str = document.getElementById('head').innerHTML;

    // result = new String(str);
    // result = String(str);

    // result = str.charAt(9);              повертає символ за індексом
    // result = str[9];                     аналогічно
    // result = str.charCodeAt(15);         повертає код символа за індексом
    // result = str.concat('test');         конкатенація
    // result = String.fromCharCode(302);   повертає символ за кодом
    // result = str.indexOf('з 1');         повертає індекс першого входження
    // result = str.lastIndexOf('з 1');     з кінця
    // result = str.length;                 довжина рядка
    // result = str.localeCompare('А');

    // result = str.match(/\d+/g);          всі числа в масив (регулярний вираз)
    // result = str.replace(/\d+/g, 'заміна');  замінюємо всі числа
    // result = str.search(/\d+/);         позиція першого співпадіння за регулярним виразом

    // result = str.slice(3, 15);           повертая підрядок
    // result = str.split(' ');             розбиває рядок на масив
    // result = str.substr(3, 1);           повертає фрагмент рядка починаючи з 3 (14 символів)
    // result = str.toLocaleLowerCase();    нижній регістр
    // result = str.toLocaleUpperCase();    верхній регістр
    // result = str.toLocaleUpperCase();

    document.getElementById('result').innerHTML = result;
}

// ---------- lesson 8 ----------


// ---------- lesson 9 ----------

function numeric_work() {
    // result = Math.abs(-5);               модуль
    // result = Math.acos(-0.954);          arccos
    // result = Math.asin(-0.954);          arcsin
    // result = Math.atan(-6.954);          arctan
    // result = Math.ceil(0.146);           округлення до більшого
    // result = Math.cos(45 * 0.01745);     cos 45*
    // result = Math.E;                     константа
    // result = Math.exp(2);                експонента
    // result = Math.floor(3.9);            округлення до меншого
    // result = Math.LN10;                  константа
    // result = Math.LN2;                   константа
    // result = Math.log(64);               логарифм
    // result = Math.LOG10E;                константа
    // result = Math.LOG2E;                 константа
    // result = Math.max(10, 43, 564);      максимальне число з переданих
    // result = Math.min(10, 43, 564);      мінімальне число
    // result = Math.pow(10, 3);            10^3
    // result = Math.random();              0.0 1.0
    // result = Math.floor(Math.random()*100)-50;       -50  50
    // result = Math.sin(30 * (2 * Math.PI / 360));     sin 30*
    // result = Math.sqrt(81);              квадратний корінь
    // result = Math.SQRT1_2;               1 / sqrt(2)
    // result = Math.SQRT2;                 sqrt(2)
    // result = Math.tan(45*(2*Math.PI/360));           tan 1
    // Number.MAX_VALUE;
    // Number.MIN_VALUE;
    // Number.NEGATIVE_INFINITY;
    // Number.POSITIVE_INFINITY;
    // Number.NaN;

    // result = (4.6).toFixed(3);           4.600
    // result = (4.6).toPrecision(1);       5

    // result = parseFloat('45.6575dsfsdf');
    // result = parseInt('45.6575 dsfsdf');

    document.getElementById('result').innerHTML = result;
}

// ---------- lesson 9 ----------

// ---------- lesson 10 ----------

// var obj = {  };

/*
var obj1 = {
    One : 'test',
    'dsfsd' : 213,
    213 : 'asd'
};
*/

// console.log(obj1);

// date = new Date();
// arr = [];

// obj2 = new Object();

/*
obj = {
    'info' : 'test1',
    'info test' : 'test'
};
*/

// console.log(obj.info);
// console.log(obj['info test']);

// obj.info = 'test edit';

function addObj(object, property, value) {
    return object[property] = value;
}

// addObj(obj, 'test', 214);
// obj.gfd = 123;

/*
a = {'test' : 5};
b = a;
b.test = 21;

b = Object.create(a);

console.log(a);
*/

obj = {
    'gftest'  : 21,
    'fdsf'  : 324,
    432     : 15,
    'test' : {
        'ds' : 2,
        'dfs' : 4
    }
};

// console.log(obj);

function iter(object) {
    for (property in object) {
        if(typeof object[property] === 'object'){
            iter(object[property]);
        } else {
            console.log(property + " -  " + object[property]);
        }
    }
}

// ---------- lesson 10 ----------


// ---------- lesson 11 ----------

// if(obj.test) console.log('yes');
// if('test' in obj) console.log('yes');
// if(obj.hasOwnProperty('test')) console.log('yes');
// if(obj.propertyIsEnumerable('test')) console.log('yes');
// if(obj.test !== undefined) console.log('yes');

obj1 = {
    'a' : 10,
    'b' : 20,
    'func' : function () {
        console.log(this.a + this.b);
    },
    get sum() {
        return this.a + this.b;
    },
    set sum(value) {
        this.a += value;
    }
};

obj1.func2 = function () {
    console.log('test')
};

Object.defineProperty(obj1, 'test', {
    value: 100,
    writable: true,
    enumerable: true,
    configurable: true
});

// obj1.sum = 6;
// console.log(obj1);

function People(name, age, id) {
    this.name = name;
    this.age = age;
    this.id = id;
}

// People.prototype = obj;

obj2 = new People('Vanya', 20, 14235);
// console.log(obj2);

// ---------- lesson 11 ----------


// ---------- lesson 12 ----------

// window.location = 'http://google.com';
// window.alert('hello');
// window.document.getElementById('test');

// window.setTimeout();
// window.setInterval();

function window_test() {
    wrap = document.getElementById('popup_overlay');

    close_button = document.getElementById('popup_close');
    close_button.onclick = popup_close;

    open_button = document.getElementById('popupIn');
    open_button.onclick = popup;

    var tIn, tOut;

    function popup() {
        wrap.style.display = 'block';
        popup_in(1);
    }

    function popup_in(opacity) {
        op = (wrap.style.opacity) ? parseFloat(wrap.style.opacity) : 0;

        if(op < opacity) {

            clearTimeout(tOut);

            op += 0.05;
            wrap.style.opacity = op;
            tIn = setTimeout(popup_in, 25, opacity)
        }
    }

    function popup_out(opacity) {
        op = (wrap.style.opacity) ? parseFloat(wrap.style.opacity) : 1;

        if(op > opacity) {

            clearTimeout(tIn);

            op -= 0.05;
            wrap.style.opacity = op;
            tOut = setTimeout(popup_out, 25, opacity)
        }

        if(wrap.style.opacity == opacity) {
            wrap.style.display = 'none';
        }
    }

    function popup_close() {
        popup_out(0);
    }

    h1 = document.getElementById('head-h1');
    h1.onclick = function () {
        clearInterval(intStop);
    };

    function change_color() {
        if(h1.style.color === 'black') {
            h1.style.color = 'white';
        }
        else {
            h1.style.color = 'black';
        }
    }

    intStop = setInterval(change_color, 400);

    do {
        str = prompt('Введіть повідомлення');
        result = confirm('Ви ввели: ' + str + '. Натисніть ОК для продовження або ВІДМІНА для повторення');

    }
    while (!result);
}

// ---------- lesson 12 ----------


// ---------- lesson 13 ----------

function window2_test() {

    w1 = null;

    open_button = document.getElementById('openWindow');
    open_button.onclick = function () {
        w1 = window.open('w1.html', 'w1', 'width=420,height=200,resizable=no,scrollbars=yes,status=no,left=500px,top=400px,toolbar=no,location=no');

        console.log(w1.opener);
    };

    close_button = document.getElementById('closeWindow');
    close_button.onclick = function () {
        if(typeof w1 === 'object') {
            w1.close();
            // console.log(w1.closed);
        }
    };

    myVar = 'Hello world';

    function getMyVar() {
        /*
        alert(myVar);
        w1.editMyVar();
        alert(myVar);
        */

        // fr1 = document.getElementById('f1');
        // console.log(fr1.contentWindow);
        // fr1.contentWindow.getAlert();

        // window.frames['frame1'].getAlert();
        // window.frames['frame1'].getParentFunc();

        // w1.focus();
        // w1.print();
        // w1.scrollBy(50, 50);
        // w1.scrollTo(50, 50);
        // w1.moveBy(50, 50);
        // w1.moveTo(0, 0);

        // w1.resizeBy(-20, -20);
        // w1.resizeTo(300, 300);

        // console.log(w1.innerHeight + ' | ' + w1.innerWidth);
        console.log(w1.outerHeight + ' | ' + w1.outerWidth);
    }

    func_button = document.getElementById('function');
    func_button.onclick = function () {
        getMyVar();
    };


    // console.log(window.name);
}

function myOpen() {
    console.log('myOpen');
}

// ---------- lesson 13 ----------


// ---------- lesson 14 ----------

function lesson14() {
    // window.location = 'https://google.com';
    // console.log(window.location);
    // console.log(location);
    // console.log(document.location);
    // console.log(location.href);
    // console.log(location.toString());

    function urlArgs() {
        args = {};
        query = location.search.substring(1);
        parts = query.split("&");

        for (i = 0; i < parts.length; i++) {

            pos = parts[i].indexOf('=');

            if(pos === -1){
                continue;
            }

            property = parts[i].substring(0, pos);
            value = parts[i].substring(pos+1);
            args[property] = value;
        }

        return args;
    }

    // console.log(urlArgs());

    btn = document.getElementById('test14');
    btn.onclick = function () {
        // location.assign('https://google.com');
        // location.replace('https://google.com');
        // location.reload();

        // console.log(history.length);
        // history.back();
        // history.forward();
        // history.go(-3);

        // console.log(navigator);
        // console.log(screen);
    }
}

// ---------- lesson 14 ----------


// ---------- lesson 15 ----------

function lesson15() {

    // test = document.getElementById('test');

    function getElements(){

        elements = {};
        for (i = 0; i < arguments.length; i++){

            id = arguments[i];
            element = document.getElementById(id);
            if (element === null) {
                continue;
            }
            elements[id] = element;
        }
        return elements;

    }
    // elements = getElements('test', 'result', 'clock', 'countdown');

    // test = document.getElementsByName('test');
    // test = document.getElementsByClassName('test class');
    // test = document.getElementsByTagName('div');
    // wrap = document.getElementById('wrap');
    // test = wrap.getElementsByTagName('p');

    // test = document.images;
    // test = document.links;
    // test = document.forms;

    // test = document.getElementById('test').className;

    // console.log(test);
}

// ---------- lesson 15 ----------


// ---------- lesson 16 ----------

function lesson16() {
    // test = document.querySelectorAll('#head-h1');
    // test = document.querySelectorAll('.test');
    // test = document.querySelector('.test');
    // test = document.querySelector('.carcass h1');
    // test = document.querySelector('.carcass').querySelector('h1');

    document.querySelector('#head-h1').onclick = function () {
        test = document.querySelector('.carcass');

        // console.log(test.parentNode);
        // console.log(test.childNodes);
        // test.parentNode.childNodes[1].style.border = '2px solid red';
        // test.parentNode.firstChild.nextSibling.style.border = '2px solid red';
        // console.log(test.firstChild.nextSibling.nextSibling.nextSibling.firstChild.firstChild.nodeValue = 'test');

        // console.log(test.nodeType);
        // console.log(test.nodeName);
        // console.log(test.nodeName);

        // console.log(test.children);
        // console.log(test.firstElementChild);
        // console.log(test.lastElementChild);
        // console.log(test.lastElementChild.nextElementSibling);
        // console.log(test.lastElementChild.previousElementSibling);
        // console.log(test.childElementCount);

        // console.log(test.id = 'new_id');

        // console.log(document.forms[0].id);
        // console.log(document.links[0].href='test');

        // test.setAttribute('test', 'test');
        // test.getAttribute('test');
        // test.hasAttribute('test');
        // test.removeAttribute('test');
    };

    // console.log(test);
}

// ---------- lesson 16 ----------


// ---------- lesson 17 ----------

function lesson17() {
    document.querySelector('#head-h1').onclick = function () {
        test = document.querySelector('.carcass h2');
        // console.log(test.innerHTML);
        // console.log(test.outerHTML);
        // console.log(test.insertAdjacentHTML('beforebegin', '<strong>Some text</strong>'));
        // console.log(test.insertAdjacentHTML('afterbegin', '<strong>Some text</strong>'));
        // console.log(test.insertAdjacentHTML('beforeend', '<strong>Some text</strong>'));
        // console.log(test.insertAdjacentHTML('afterend', '<strong>Some text</strong>'));

        test = test.firstChild;

        // console.log(test.textContent);
        // console.log(test.innerText);         IE

        function textContent(element, value) {
            content = element.textContent;

            if(value === undefined) {
                if (content !== undefined)
                    return content;
                else
                    return element.innerText
            }
            else {
                if (content !== undefined)
                    return element.textContent = value;
                else
                    return element.innerText = value
            }
        }

        // textContent(test, '2141234');
    };

    btn = document.getElementById('button');

    btn.onclick = function () {
        newDiv = document.createElement('div');
        newText = document.createTextNode('some string');
        newComment = document.createComment('comment');
        documentFragment = document.createDocumentFragment();

        copyButton = btn.cloneNode();
        copyDiv = document.querySelector('.wrap').cloneNode(true);

        newDiv.appendChild(newText);

        div = document.querySelector('.carcass');
        // div.insertBefore(newDiv, document.querySelector('#clock'))
        // div.insertBefore(document.querySelector('h2'), null);
        // div.removeChild(document.querySelector('h2'));
        div.replaceChild(newDiv, document.querySelector('h2'));

        // console.log(newDiv);
    }
}

// ---------- lesson 17 ----------


// ---------- lesson 18 ----------

function lesson18(){
    form = document.getElementById("firstForm");
    email = document.getElementById("exampleInputEmail");
    checkboxes = document.querySelectorAll("#firstForm input[type=checkbox]");

    /*
    for(i = 0; i < checkboxes.length; i++) {
        console.log(checkboxes[i].type);
    }
    */

    // console.log(window.myForm);
    // console.log(document.myForm);
    // console.log(document.firstForm);
    // console.log(document.forms[1]);
    // console.log(document.forms.myForm);
    // console.log(document.forms.firstForm);
    // console.log(document.forms.firstForm.email);
    // console.log(document.forms.firstForm.email.value);
    // console.log(document.forms.firstForm.option);
    // console.log(document.forms.firstForm.elements);
    // console.log(document.forms.firstForm.elements.length);
    // console.log(document.forms.firstForm.length);

    /*
    option = document.forms.firstForm.elements.option;
    for(i = 0; i < option.length; i++) {
        if(option[i].checked)
            console.log(option[i].value);
    }
    */

    form = document.forms.firstForm;

    // console.log(form.action);
    // console.log(form.method);
    // console.log(form.encoding);

    // form.submit();
    // form.reset();

    // console.log(email.type);
    // console.log(email.form);
    // console.log(email.name);
    // console.log(email.value);
    // console.log(email.value = 'test');

    // option = document.forms.firstForm.elements.option;
    /*
    for(i = 0; i < option.length; i++) {
        if(option[i].defaultChecked)
            console.log(option[i].value);
    }
    */

    // console.log(email.placeholder);

    select = form.elements.myselect;
    // console.log(select.type);            // select-multiple / select-one
    // console.log(select.options);
    // console.log(select.selectedIndex);   // один вибраний елемент

    /*
    newOption = new Option(
        "Option new",
        10,
        false,
        false
    );

    select[select.length] = newOption;
    */
}

// ---------- lesson 18 ----------


// ---------- lesson 19 ----------

function lesson19() {

    btn = document.querySelector('.btn-default');
    /*
    btn.onclick = function () {
        document.forms[1].submit();
    };
    */

    func = function (event) {
        // document.forms[1].submit();

        form = document.forms.myForm;
        empty = false;

        for (i=0; i < form.length; i++) {
            if((form.elements[i].type === "text" || form.elements[i].type === "password") && form.elements[i].value === "") {
                form.elements[i].style.borderColor = "red";
                empty = true;
            }
        }

        if (empty)
            alert("Заповніть всі поля");
        else {
            form.submit();
        }
    };

    /*
    btn.addEventListener('click', func);

    query = location.search;

    if (query !== '') {
        btn.removeEventListener('click', func)
    }
    */

    if(btn.addEventListener) {
        btn.addEventListener('click', func);
    }
    else if(btn.attachEvent){
        btn.attachEvent('onclick', func);
    }

}

// ---------- lesson 19 ----------


// ---------- lesson 20 ----------

function lesson20() {
    btn = document.getElementById('my-btn');
    btn.addEventListener('click', function (ev) {
        // console.log(ev);
        // console.log(ev.type);
        // console.log(this);
        ev.preventDefault();
    });

    emailInput = document.getElementById('exampleInputEmail');
    /*
    emailInput.addEventListener('keypress', function (ev) {
        if (ev.charCode === 100)
            return false;
        else {
            console.log(ev.charCode);
            return true;
        }
    });
    */
    /*
    emailInput.onkeypress = function (ev) {
        if (ev.charCode === 100)
            return false;
        else {
            console.log(ev.charCode);
            return true;
        }
    };
    */

    /*
    window.onbeforeunload = function (ev) {
        message = "Hello world";
        return message;
    }
    */
}

// ---------- lesson 20 ----------


// ---------- lesson 21 ----------

function lesson21() {
    div = document.getElementById('one');
    div.addEventListener('click', function (ev) {
        this.style.backgroundColor = "#cccccc";
        console.log(this.tagName);
        if(ev.target.tagName === 'SPAN')
            ev.stopPropagation();
    }, true);

    p = document.getElementById('two');
    p.addEventListener('click', function (ev) {
        this.style.backgroundColor = "#00cc00";
        console.log(this.tagName);
        // ev.stopPropagation();
        // ev.stopImmediatePropagation();
    }, true);

    span = document.getElementById('three');
    span.addEventListener('click', function (ev) {
        this.style.backgroundColor = "#cc0000";
        console.log(this.tagName);
    }, true);

    /*
    form = document.getElementById('firstForm');
    form.addEventListener('click', function (ev) {
        console.log(ev.target);
        ev.target.style.border = "1px solid red";
        console.log(this);
    });
    */
}

// ---------- lesson 21 ----------


// ---------- lesson 22 ----------

function lesson22(element, event) {
    // btn = document.getElementById('my-btn');
    // btn.addEventListener('click', function (ev) {
    // btn.addEventListener('dblclick', function (ev) {
    // btn.addEventListener('mousemove', function (ev) {
    // btn.addEventListener('mousedown', function (ev) {
    // btn.addEventListener('mouseup', function (ev) {
    // btn.addEventListener('mouseover', function (ev) {
    /*
    btn.addEventListener('mouseout', function (ev) {
        // ev.preventDefault();
        // console.log(ev.clientX);
        console.log(ev);
    });
    */

    function drag(element, event) {
        /*
        scroll = getScroll();

        startX = event.clientX + scroll.x;
        startY = event.clientY + scroll.y;
        */

        startX = event.pageX;
        startY = event.pageY;

        elementX = element.offsetLeft;
        elementY = element.offsetTop;

        deltaX = startX - elementX;
        deltaY = startY - elementY;

        if(document.addEventListener) {
            document.addEventListener('mousemove', move_mouse, true);
            document.addEventListener('mouseup', up_mouse, true);
        }

        if(event.stopPropagation) {
            event.stopPropagation();
        }

        if(event.preventDefault) {
            event.preventDefault();
        }

        function move_mouse(ev) {
            element.style.left = (ev.pageX - deltaX) + "px";
            element.style.top = (ev.pageY - deltaY) + "px";

            if(ev.stopPropagation) {
                ev.stopPropagation();
            }
        }

        function up_mouse(ev) {
            if(document.removeEventListener) {
                document.removeEventListener("mouseup", up_mouse, true);
                document.removeEventListener("mousemove", move_mouse, true);
            }

            if(ev.stopPropagation) {
                ev.stopPropagation();
            }
        }
    }

    function getScroll(w) {
        w = w || window;
        if(w.pageXOffset != null) {
            return {'x' : w.pageXOffset, 'y' : w.pageYOffset};
        }
    }

    drag(element, event);
}

// ---------- lesson 22 ----------


// ---------- lesson 23 ----------

function lesson23() {
    input = document.getElementById('exampleInputEmail');

    /*
    input.addEventListener('keypress', function (ev) {
        // console.log(String.fromCharCode(ev.charCode));
        // ev.preventDefault();
        // return false;

        code = ev.charCode || ev.keyCode;
        if (code < 32 || ev.ctrlKey || ev.altKey) {
            return false;
        }

        text = String.fromCharCode(code);
        chars = ev.target.getAttribute('data-chars');

        if(chars.indexOf(text) === -1) {
            alert('Недопустимий символ - ' + text);
            ev.preventDefault();
            return true;
        }
    });
    */

    /*
    input.addEventListener('keydown', function (ev) {
        console.log(ev);
    });
    */

    btn = document.getElementById('my-btn');
    form = document.getElementById('firstForm');

    /*
    btn.addEventListener('click', function (ev) {
       form.submit();
    });
    */

    inputs = document.getElementsByTagName('input');

    for (i = 0; i < inputs.length; i++) {
        var element = inputs[i];
        if(element.type === 'radio') {
            element.addEventListener('change', function (ev) {
                id = ev.target.getAttribute('value');

                if(id) {
                    div = document.querySelector('.extraFields');
                    divs = div.children;

                    for (j = 0; j < divs.length; j++) {
                        if(divs[j].lastElementChild.getAttribute('id') === id) {
                            divs[j].className = 'active';
                        } else {
                            divs[j].className = 'hidden';
                        }
                    }
                }

            });
        }
    }

}

// ---------- lesson 23 ----------


// ---------- lesson 24 ----------

function lesson24() {

    btn = document.getElementById('my-btn');

    btn.addEventListener('click', function (ev) {
        ev.preventDefault();

        // request.open("GET", "server.php");
        request = new XMLHttpRequest();
        request.open("POST", "server.php");
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')

        request.addEventListener('readystatechange', function (e) {
            if(request.readyState === 4 && request.status === 200) {
                // alert('success');
                document.getElementById('three').innerHTML = request.responseText;
            }
        });

        request.send("name=Vanya");
    });
}

// ---------- lesson 24 ----------


window.onload = function () {
    display_time();
    // countdown();
    // string_work();
    // numeric_work();
    // iter(obj);
    // window_test();
    // window2_test();
    // lesson14();
    // lesson15();
    // lesson16();
    // lesson17();
    // lesson18();
    // lesson19();
    // lesson20();
    // lesson21();
    // lesson22();
    // lesson23();
    // lesson24();
};