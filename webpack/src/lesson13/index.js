var User = /** @class */ (function () {
    function User() {
        this.name = 'Vanya test';
        this.age = 20;
    }
    User.prototype.logInfo = function () {
        console.log(this.name, this.age);
    };
    return User;
}());
//# sourceMappingURL=index.js.map