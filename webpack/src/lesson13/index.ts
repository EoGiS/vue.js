interface Person {
    name: string;
    age: number;
}

class User implements Person {
    age: number;
    name: string;

    constructor() {
        this.name = 'Vanya test';
        this.age = 20;
    }

    private logInfo() {
        console.log(this.name, this.age);
    }
}