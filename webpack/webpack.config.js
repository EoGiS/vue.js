const path = require('path');
const CaseSensitivePlugin = require('case-sensitive-paths-webpack-plugin');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        // index: './src/init.js',
        // shop: './src/lesson12/shop.js',
        // profile: './src/lesson12/profile.js',
        // vendor: ['jquery', 'lodash'],
        index: './src/lesson20/index',
        style: './src/lesson20/style.css',
        // vendor: ['jquery']
    },
    output: {
        // path: path.resolve(__dirname, 'dist'),
        path: path.resolve(__dirname, 'dist/lesson20'),
        // filename: 'bundle.js'
        filename: '[name].js'
    },
    mode: 'development',
    resolve: {
        extensions : ['.js', '.ts']
    },
    watch: false,
    devtool: 'source-map',
    plugins: [
        new CaseSensitivePlugin(),
        new webpack.DefinePlugin({
            VERSION: JSON.stringify("0.0.1"),
            PRODUCTION: false,
            HTML5_SUPPORT: true
        }),
        /*
        new webpack.ProvidePlugin({
            $: 'jquery'
        })
        */
        /*
        new HtmlWebpackPlugin({
            title: 'Test HTML WebPack',
            hash: true,
            // minify: true
            template: './src/lesson11/template.html'
        })
        */
        /*
        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true
        }),
        */
        new HtmlWebpackPlugin({
            title: 'WebPack Dev Server'
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    /*
    optimization: {
        splitChunks: {
            minSize: 1,
            cacheGroups: {
                commons: {
                    name: 'commons',
                    chunks: 'all',
                    minChunks: 2
                },
                vendor: {
                    test: /node_modules/,
                    chunks: 'initial',
                    name: 'vendor',
                    enforce: true
                },
            }
        }
    }
    */
    module: {
        rules: [
            /*
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader',
            },
            */
            /*
            {
                test: /\.css$/,
                // use: ['style-loader', 'css-loader'],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                }),
            },
            */
            /*
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            */
            /*
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'less-loader'],
                }),
            },
            */
            /*
            {
                test: require.resolve('jquery'),
                loader: 'expose-loader?$',
            },
            */
            /*
            {
                test: /\.js$/,
                loader: 'strip-loader',
                options: {
                    strip: ['console.*', 'alert']
                }
            },
            */
            /*
            {
                test: /\.png$/,
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[ext]?[hash]'
                }
            }
            */
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
    devServer: {
        hot: true,
    }
};