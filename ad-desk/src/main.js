import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import BuyModalComponent from '@/components/shared/BuyModal'
import firebase from 'firebase/app'
import 'vuetify/dist/vuetify.min.css'
import './stylus/main.styl'
// import colors from 'vuetify/es5/util/colors'

/*
Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken1
  }
})
*/
Vue.use(Vuetify)
Vue.component('app-buy-modal', BuyModalComponent)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyAQmh5pQqTk6NiWSZu-KkOFV6npLBL-lm4',
      authDomain: 'ad-desk.firebaseapp.com',
      databaseURL: 'https://ad-desk.firebaseio.com',
      projectId: 'ad-desk',
      storageBucket: 'ad-desk.appspot.com',
      messagingSenderId: '936581297247'
    })

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    })

    this.$store.dispatch('fetchAds')
  }
})
